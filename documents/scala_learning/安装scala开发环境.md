mac 下idea安装scala插件

点击preferences > plugins > 点击安装scala插件，因为网络原因失败的话就从idea的plugin官网下载scala.zip的插件安装包
选择和idea匹配的版本我的是idea201903 scala2.13.2

新建一个maven项目然后选择在项目文件右健framework支持，选择scala和maven

这里还有一个坑是新建一个scala文件夹，右键，mark as source root否则会爆class not found 的编译错误


mac 下面terminal安装scala

安装mac homebrew

```bash

/bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
```


```bash
brew install scala
brew install sbt
```

修改镜像源
```$xslt

vim ~/.sbt/repositories
```


```bash
[repositories]
local
huaweicloud-maven: https://repo.huaweicloud.com/repository/maven/
maven-central: https://repo1.maven.org/maven2/
sbt-plugin-repo: https://repo.scala-sbt.org/scalasbt/sbt-plugin-releases, [organization]/[module]/(scala_[scalaVersion]/)(sbt_[sbtVersion]/)[revision]/[type]s/[artifact](-[classifier]).[ext]
```



