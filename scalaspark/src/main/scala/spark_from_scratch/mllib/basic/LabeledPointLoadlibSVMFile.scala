package spark_from_scratch.mllib.basic

import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.{SparkConf, SparkContext}

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 01:22
 * @version 1.0
 */
object LabeledPointLoadlibSVMFile {
  def main(args: Array[String]) {
    val conf = new SparkConf().setMaster("local").setAppName(this.getClass().getSimpleName().filter(!_.equals('$')))
    //  println(this.getClass().getSimpleName().filter(!_.equals('$')))
    //设置环境变量
    val sc = new SparkContext(conf)

    val mu = MLUtils.loadLibSVMFile(sc, "file/data/mllib/input/basic/sample_libsvm_data.txt") //读取文件
    mu.foreach(println) //打印内容

    sc.stop
  }
}
