package spark_from_scratch.graphx.learning

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 4:17 下午
 * @version 1.0
 */
object testSortBy {

  def main(args: Array[String]): Unit = {
    var arr = Array((1, 2), (2, 1), (3, 5), (4, 4))
    arr.sortBy(_._2).reverse.foreach(println)

  }

}
