package spark_from_scratch.basic

import org.apache.spark.{SparkConf, SparkContext}

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 3:33 下午
 * @version 1.0
 */
object WordCountOnSPark {
  def main(args: Array[String]) {

    if (args.length < 1) {
      System.err.println("Usage: <file>")
      System.exit(1)
    }

    val conf = new SparkConf()
    conf.setAppName("SparkWordCount")

    val sc = new SparkContext(conf)

    val rdd = sc.textFile(args(0))

    rdd.flatMap(_.split(" ")).map((_, 1)).reduceByKey(_ + _).map(x => (x._2, x._1)).sortByKey(false).map(x => (x._2, x._1)).saveAsTextFile(args(1))

    sc.stop
  }
}