package spark_from_scratch.basic

import org.apache.spark.mllib.linalg.Vectors

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 01:06
 * @version 1.0
 */
object VectorLearning {
  def main(args: Array[String]) {

    val vd = Vectors.dense(2, 0, 6)
    println(vd(2))
    println(vd)

    //数据个数，序号，value
    val vs = Vectors.sparse(4, Array(0, 1, 2, 3), Array(9, 5, 2, 7))
    println(vs(2))
    println(vs)

    val vs2 = Vectors.sparse(4, Array(0, 2, 1, 3), Array(9, 5, 2, 7))
    println(vs2(2))
    println(vs2)



  }
}
