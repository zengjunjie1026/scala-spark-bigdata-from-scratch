package spark_from_scratch.basic

import java.text.SimpleDateFormat
import java.util.Date

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 3:32 下午
 * @version 1.0
 */

object TestDate {
  def main(args: Array[String]) {
    //
    //  val sdf = new SimpleDateFormat("yyyy-MM-dd H:mm:ss")
    //
    val iString=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date() );
    val s1="file/data/examples/output/"+iString;
    println(s1)
    val s0= "file/data/examples/input/*"
    println(s0)
  }
}
