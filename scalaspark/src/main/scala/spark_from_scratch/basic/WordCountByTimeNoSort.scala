package spark_from_scratch.basic

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.SparkContext

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 3:32 下午
 * @version 1.0
 */
object WordCountByTimeNoSort {
  def main(args: Array[String]) {
    val s0="local"
    val sinput= "file/data/examples/input/wordCount/*"
    val iString=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date() )
    val soutput="file/data/examples/output/wordCount" + iString;

    if (args.length > 0 ){
      println("usage is org.test.WordCount <master> <input> <output>")
      return
    }
    val sc = new SparkContext(s0, "WordCount",
      System.getenv("SPARK_HOME"), Seq(System.getenv("SPARK_TEST_JAR")))
    val textFile = sc.textFile(sinput)
    val result = textFile.flatMap(line => line.split("\\s+"))
      .map(word => (word, 1)).reduceByKey(_ + _)
    //        result.count();
    result.saveAsTextFile(soutput)
    println("end");
    sc.stop
  }
}