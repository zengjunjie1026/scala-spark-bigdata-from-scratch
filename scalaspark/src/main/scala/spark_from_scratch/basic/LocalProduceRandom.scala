package spark_from_scratch.basic

import java.io.{File, PrintWriter}
import java.text.SimpleDateFormat
import java.util.{Date, Random}

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 3:24 下午
 * @version 1.0
 */
object LocalProduceRandom {
  def main(args: Array[String]) {
    val n = 100000
    val m = 100
    val iString = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date())
    val soutput = "file/data/examples/output/" + iString;
    val w1 = new PrintWriter(new File("file/data/examples/output/output" + iString + ".txt"))
    var uu = new Random()
    for (i <- 1 to n) { w1.println(uu.nextInt(m)) }
    println("success")
    w1.close()

    //    var count = 0
    //    for (i <- 1 to n) {
    ////      val x = random * 2 - 1
    ////      val y = random * 2 - 1
    ////      if (x*x + y*y < 1) count += 1
    //    }
    //    println("Pi is roughly " + 4 * count / 100000.0)
  }
}