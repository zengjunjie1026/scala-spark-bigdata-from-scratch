package spark_from_scratch.basic

import org.apache.spark.SparkContext

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 3:30 下午
 * @version 1.0
 */
object test2 {
  def main(arg:Array[String]){
    val sc = new SparkContext("local", "testpara",System.getenv("SPARK_HOME"), Seq(System.getenv("SPARK_TEST_JAR")))
    val collection=sc.parallelize(1 to 10000000)
    println("end");
    sc.stop
  }
}