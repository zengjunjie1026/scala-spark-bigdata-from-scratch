package scala_from_scratch

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 00:53
 * @version 1.0
 */
object HelloScala {
  def main(args: Array[String]): Unit = {
    println("hello scala")
  }

}
