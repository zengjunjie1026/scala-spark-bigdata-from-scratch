package scala_from_scratch

/**
 * @author andrew
 * @email zengjunjie1026@163.com
 * @date 2020/9/3 01:02
 * @version 1.0
 */
object ScalaBasic{
  def main(args: Array[String]): Unit = {
    println("hello scala")

    //  println(add(2,4))


    //   println(add2(23)(34))

    //34
    //    var i=0
    //    while (i <1555) {
    //      println(i)
    //      i+=1
    //    }
    //
    //    for (j <-1 to 10000){
    //      println(j)
    //
    //
    //    }
    //
    //
    //    args.foreach(println)
    //
    //    for (i <- 1 to 10 if i % 2 == 0)
    //      println(i)

    //   val greetStrings = new Array[String](3)
    //    greetStrings(0) = "Hello"
    //    greetStrings(1) = ", "
    //    greetStrings(2) = "world!\n"
    //
    //
    //    println(greetStrings)
    //
    //    greetStrings.foreach(println)
    //
    //
    //    val numNames = Array("zero", "one", "two")
    //
    //
    //    println(numNames.length)
    //    print(numNames.foreach(println))

    val list1 = List(1, 2)
    val list2 = List(3, 4)
    val list3 = list1 ::: list2  //list3结果为(1,2,3,4)
    val list4 = 0 :: list3       //list4结果为(0,1,2,3,4)

    list3.foreach(println)

    var rr2 = Array(23,221,22,1,2,34)

    var r3 = sortScala(rr2)

    r3.foreach(println)
    println("8" * 20)


    list4.foreach(println)



    println("1000" * 29)


    // 调用方法
    val a = ApplyTest() // 类名+括号，调用对象的apply方法


    // print(factorial(23))





    //mul(23,56)
    //    mulCurry(334)
    //    mulCurry(3344)
  }



  def sortScala(xs: Array[Int]): Array[Int] = {
    if (xs.length <= 1) xs
    else {
      val pivot = xs(xs.length / 2)
      Array.concat(
        sortScala(xs filter (pivot >)),
        xs filter (pivot ==),
        sortScala(xs filter (pivot <)))
    }
  }


  //  def factorial(n: Int): Int = {
  //    @annotation.tailrec
  //    def go(n: Int, acc: Int): Int =
  //      if (n <= 0) acc
  //      else go(n - 1, n * acc)
  //
  //    go(n, 1)
  //  }

  def add = (x : Int, y : Int) => x + y
  def add2(x : Int)(y : Int) = x + y
  //
  //  def mul(x: Int, y: Int) = x * y println(mul(10, 10))
  //  def mulCurry(x: Int) = (y: Int) => x * y println(mulCurry(10)(9))
  //  def mulCurry2(x: Int)(y:Int) = x * y println(mulCurry2(10)(8))
}


class Person {
  var email = "abc123@126.com" // 变量，var声明会生成getter和setter方法
  var name : String = _ // 变量， _起到占位符的作用
  val age = 10;         // 常变量，val声明只会生成getter
  private val gender = "male" //只能在类内部使用
}


class Student(var name : String, val number : String) {
  println("主构造器！")
}

class Student2(var name : String, val number : String) {
  println("主构造器！")
  var gender : String = _
  def this(name : String, number : String, gender : String) {
    this(name, number)
    this.gender = gender
  }
}


class ApplyTest {
  println("Test")
}

object ApplyTest {
  def apply() = new ApplyTest
}

