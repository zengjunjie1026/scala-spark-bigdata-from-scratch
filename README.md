# scala-spark-bigdata-from-scratch

#### 介绍
从零开始学习scala spark 以及大数据组建

#### 软件架构
软件架构说明

本repo分为文档 代码


#### 安装教程

1.  git clone https://gitee.com/zengjunjie1026/scala-spark-bigdata-from-scratch.git
2.  xxxx
3.  xxxx

#### 使用说明

1.  jdk 11.0.2
2.  hadoop为分布式系统本地安装
3.  spark2.4
4.  flink 1.9

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
